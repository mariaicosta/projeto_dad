require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Home from './components/homepage'
Vue.component('home', Home)


const routes = [
	{path:'/', component:Home}
]


const router = new VueRouter({
	routes //nome e propriedade forem os mesmos podemos omitir o valor
})



const app = new Vue({
    el: '#app',
    router,
    data: {
   
    },
    methods: {
        
    },
    mounted() {
        
    }
});