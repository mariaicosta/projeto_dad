<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model{
	protected $fillable = [
		'id',
        'email',
        'balance',
    ];

    public $incrementing = false;

    public function user(){
    	return $this.hasOne('App\User', 'id', 'id');
    }

}